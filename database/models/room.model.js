const mongoose = require("mongoose");
const schema = mongoose.Schema;

const roomSchema = schema({
  index: Number,
  title: String,
});

const Room = mongoose.model("room", roomSchema);

module.exports = Room;
