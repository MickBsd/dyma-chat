const userRoutes = require("./user.routes");
const router = require("express").Router();
const auth = require("@dyma/auth");

router.use("/users", userRoutes);

router.get("/", auth, (req, res) => {
  res.render("index", { user: req.user });
});

router.get("*", (req, res) => res.redirect("/"));

module.exports = router;
