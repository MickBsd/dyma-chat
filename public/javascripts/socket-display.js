function createRoomItem(room, isActive) {
  const li = document.createElement("li");
  li.classList.add("item-room", "p-2", "m-2");
  if (isActive) {
    li.classList.add("active");
  }
  li.innerHTML = `
    # ${room.title}
  `;
  li.addEventListener("click", (event) => {
    if (activeRoom._id !== room._id) {
      ioClient.emit("leaveRoom", activeRoom._id);
      const activeRoomItem = document.querySelector(".item-room.active");
      activeRoomItem.classList.remove("active");
      event.target.classList.add("active");
      activateRoom(room);
    }
  });
  return li;
}

function displayRooms() {
  const roomsContainer = document.querySelector(".list-rooms");
  const items = rooms.map((room) =>
    createRoomItem(room, activeRoom._id === room._id)
  );
  roomsContainer.innerHTML = "";
  roomsContainer.prepend(...items);
}

function createMessageItem(message) {
  message.time = new Date(message.updatedAt).toLocaleTimeString();
  const li = document.createElement("li");
  li.classList.add("item-message", "d-flex", "flex-row", "mb-2");
  li.innerHTML = `
    <span class="mr-1">${message.time}</span>
    <strong class="mr-3">${message.authorName}</strong>
    <span class="flex-fill">${message.data}</span>
  `;
  return li;
}

function displayMessages() {
  const messagesContainer = document.querySelector(".list-messages");
  const items = messages.map((message) => createMessageItem(message));
  messagesContainer.innerHTML = "";
  messagesContainer.prepend(...items);
  if (items.length) {
    items[items.length - 1].scrollIntoView();
  }
}

function displayNewMessage(message) {
  const messagesContainer = document.querySelector(".list-messages");
  const newMessage = createMessageItem(message);
  messagesContainer.append(newMessage);
  newMessage.scrollIntoView();
}
