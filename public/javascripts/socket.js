let rooms = [];
let activeRoom;
let messages = [];

const ioClient = io({
  reconnection: false,
});

ioClient.on("connect", () => {
  console.log("connexion ok !");
});

ioClient.on("history", (data) => {
  messages = data;
  displayMessages();
});

ioClient.on("message", (data) => {
  messages.push(data);
  displayNewMessage(data);
});

ioClient.on("rooms", (data) => {
  rooms.push(...data);
  if (rooms.length) {
    activateRoom(rooms[0]);
    displayRooms();
  }
});

function activateRoom(room) {
  ioClient.emit("joinRoom", room._id);
  activeRoom = room;
}

setTimeout(() => {
  console.log({
    rooms,
    activeRoom,
    messages,
  });
}, 3000);
