const { Room } = require("../database/models");

exports.getRooms = () => {
  return Room.find({}).sort({ index: 1 }).exec();
};
