const socketio = require("socket.io");
const { ensureAuthenticatedOnSocketHandshake } = require("./security.config");
const { getRooms } = require("../queries/room.queries");
const {
  findMessagesPerRoomId,
  createMessage,
} = require("../queries/message.queries");
let ios;

const initSocketServer = (server) => {
  ios = socketio(server, {
    allowRequest: ensureAuthenticatedOnSocketHandshake,
  });
  ios.on("connect", async (socket) => {
    console.log("connexion ios ok");
    try {
      const rooms = await getRooms();
      socket.emit("rooms", rooms);
    } catch (e) {
      throw e;
    }
    socket.on("joinRoom", async (roomId) => {
      try {
        socket.join(`/${roomId}`);
        const messages = await findMessagesPerRoomId(roomId);
        socket.emit("history", messages);
      } catch (e) {
        throw e;
      }
    });
    socket.on("leaveRoom", (roomId) => {
      socket.leave(`/${roomId}`);
    });
    socket.on("message", async ({ text, roomId }) => {
      try {
        const { _id, firstName } = socket.request.user;
        const message = await createMessage({
          data: text,
          room: roomId,
          author: _id,
          authorName: firstName,
        });
        ios.to(`/${roomId}`).emit("message", message);
      } catch (e) {
        throw e;
      }
    });
  });

  ios.on("close", (socket) => {
    socket.disconnect(true);
  });
};

module.exports = { initSocketServer };
